# ****************************************************************************
#
# K-moyennes (K-means) : Exemple avec le jeu de données 'iris'
#
# ****************************************************************************

# Chargement des données
library(datasets)

head(iris)
str(iris)
levels(iris$Species)
table(as.integer(iris$Species), factor(iris$Species))

# Analyse exploratoire des variables
pairs(iris[c("Sepal.Length", "Sepal.Width", "Petal.Length", "Petal.Width")],
      pch=21, bg=c("red", "green", "blue")[unclass(iris$Species)])

# Selection des variables conservées
features <- c("Petal.Length", "Petal.Width")
plot(x = iris$Petal.Length, y = iris$Petal.Width,
     pch = 16,
     col = c("red", "green", "blue")[unclass(iris$Species)],
     main = "Vrais clusters")
legend(x = 1, y = 2, legend = levels(iris$Species),
       col = c("red", "green", "blue"), lty = 1)

# *****************************************************************************
# K-moyennes
# *****************************************************************************
# Modélisation K-means avec les variables 'Petal.Length' et 'Petal.Width'.
# Notons que nous aurions pu choisir 'Sepal.Width' et 'Petal.Width'
# ou encore 'Sepal.Length' et 'Petal.Width'.
# 
# Paramètres :
# centers : nombre de centres désiré
# nstart : nombre de sélections aléatoires
#          des centres de départ
set.seed(123)
model <- kmeans(iris[features], centers = 3, nstart = 10)
model

# *****************************************************************************
# Performances du classement
#
# !!! Remarque !!! :
# La précision de l'algorithme est impossible à avoir dans la réalité car
# on ne connait pas les valeurs cibles !!!
# Ceci n'est qu'une illustration !!!
# *****************************************************************************
# Affichage des labels des classes découvertes par l'algorithme
# qui peuvent ne pas correspondre aux "vrais libéllés" de classes
# car c'est un algorithme NON-SUPERVISE!!!
# Nous avons ici 1 => setosa, 2 => virginica 3 => versicolor
table(model$cluster, iris$Species)
predCluster <- as.factor(model$cluster)
head(predCluster)
levels(predCluster) = c("setosa", "virginica", "versicolor")
head(predCluster)

# Graphe des clusters prédits
par(mfrow=c(1,2)) # 1 ligne 2 colonnes
# vraies valeurs
plot(x = iris$Petal.Length, y = iris$Petal.Width,
     pch = 16,
     col = c("red", "green", "blue")[unclass(iris$Species)],
     main = "Vrais clusters")
legend(x = 1, y = 2, legend = levels(iris$Species),
       col = c("red", "green", "blue"), lty = 1)
# prédictions
plot(x = iris$Petal.Length, y = iris$Petal.Width,
     pch = 16,
     col = c("red", "blue", "green")[unclass(predCluster)],
     main = "Clusters prédits")
legend(x = 1, y = 2, legend = levels(predCluster),
       col = c("red", "blue", "green"), lty = 1)

# Calcul de la précision
library(caret)
confusionMatrix(predCluster,iris$Species)

