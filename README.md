# MOD04-TP Machine Learning

### Description

Travaux pratiques en langages **R** et **Python** de la formation **Eléments d'Apprentissage Statistique (Applications de Machine Learning avec R et Python)** :

* Régression linéaire (Linear Regression)
* Régression logistique (Logistic Regression)
* Dilemme biais-variance (Bias-Variance Dilemma)
* K-plus proches voisins (K-nearest Neighbors)
* Séparateurs à vaste marge (Support Vector Machines)
* Arbres de décision (Decision Trees)
* Agrégation de modèles (Ensemble Learning)
* Réseaux de neurones artificiels (Artificial Neural Networks)
* Centres mobiles et K-moyennes (K-Means)
* Motifs fréquents et règles d'associattion (Frequent Patterns and Association Rules).

### Auteur

Fabrice LEBEL, fabrice.lebel.pro@outlook.com, [LinkedIn](https://www.linkedin.com/in/fabrice-lebel-b850674?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BbqGekl60S9W3uZDa6jyWkg%3D%3D)

### Repo

https://bitbucket.org/fableb/mod04_tp-machine_learning